package com.calin.stormy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 50;

    private Context context;

    private TextView temperatureTextView;
    private TextView humidityTextView;
    private TextView precipitationTextView;

    private WeatherService weatherService;

    private FusedLocationProviderClient locationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        temperatureTextView = findViewById(R.id.temperatureTextView);
        humidityTextView = findViewById(R.id.humidityTextView);
        precipitationTextView = findViewById(R.id.precipitationTextView);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        weatherService = new Retrofit.Builder()
                .baseUrl("https://api.darksky.net")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherService.class);

        updateLocation();
    }

    private void onLocationChanged(Location location) {
        updateForecast(location.getLatitude(), location.getLongitude());
    }

    private void updateView(Forecast forecast) {
        temperatureTextView.setText(String.valueOf((int) forecast.getCurrently().getTemperature()));
        humidityTextView.setText(String.valueOf(forecast.getCurrently().getHumidity()));
        precipitationTextView.setText(String.valueOf(forecast.getCurrently().getPrecipProbability()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            updateLocation();
        }
    }

    private void updateLocation() {
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationProviderClient.getLastLocation()
                    .addOnSuccessListener(location -> onLocationChanged(location));
        } else {
            requestPermissions(new String[] { Manifest.permission.ACCESS_COARSE_LOCATION }, REQUEST_CODE);
        }
    }

    private void updateForecast(double latitude, double longitude) {
        weatherService.getForecast(latitude, longitude)
            .enqueue(new Callback<Forecast>() {
                @Override
                public void onResponse(Call<Forecast> call, final Response<Forecast> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        runOnUiThread(() -> updateView(response.body()));
                    }
                }

                @Override
                public void onFailure(Call<Forecast> call, Throwable t) {
                    Toast.makeText(context, "An error has occurred", Toast.LENGTH_SHORT).show();
                }
            });
    }
}
