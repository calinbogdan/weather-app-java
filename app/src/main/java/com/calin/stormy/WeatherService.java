package com.calin.stormy;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WeatherService {

    @GET("forecast/f2b01b0e372390319530c68e5645f282/{latitude},{longitude}?units=si")
    Call<Forecast> getForecast(@Path("latitude") double latitude,
                               @Path("longitude") double longitude);
}
